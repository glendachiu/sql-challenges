
#Find max. range for each date, select 3 highest ranges
CREATE TEMPORARY TABLE DatesRanges
SELECT Date, max(abs(Close-Open)) as 'Range' 
FROM ibm
GROUP by Date
ORDER by max(abs(Close-Open)) desc
limit 3;

#Find max. high for each date
CREATE TEMPORARY TABLE DatesHighs
SELECT Date, max(High) as maxHigh
FROM ibm 
WHERE Date in (SELECT Date FROM DatesRanges)
GROUP by Date;

#Match times to max. Highs
CREATE TEMPORARY TABLE HighTimes
SELECT d.Date, d.maxHigh, i.Time
from ibm i
join DatesHighs d on d.maxHigh=i.High and d.Date=i.Date;


# Show Date, max. Range, Time of max. High
SELECT dr.Date, dr.Range, ht.Time
FROM HighTimes ht
JOIN DatesRanges dr on dr.Date=ht.Date

DROP TABLE IF EXISTS DatesRanges; DROP TABLE IF EXISTS DatesHighs; DROP TABLE IF EXISTS HighTimes
