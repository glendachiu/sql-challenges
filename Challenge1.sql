# Calculate Volume Weighted Price

set @start_time := '2010-10-18 13:00:00'; # set start time (and date)
set @end_time := date_add(@start_time, INTERVAL 5 HOUR);

select sum(vol*open)/sum(vol) as VWP, 
Date_Format(date,'%d/%m/%y') as Date,
time(@start_time) as Start,
time(@end_time) as End
from prices
where date between @start_time and @end_time

