CALCULATING VOLUME Weighted Prices - Challenge 1 of 2
Click here to download sample data set

This dataset contains pricing information for Apple Computer stock across one month in 2010. Create a SQL script to determine the Volume Weighted Average Price considering the ticks in a specified 5 hour interval on any specific day.

Your output should display the following items;

The Volume Weighted Price
The date in the following format : DD / MM / YYYY
The specific 5 hour interval : Start (HH:MM) � End (HH:MM)
Challenge Tips
Below we have provided a sample VWAP calculation SQL query for a different dataset.

SELECT TRADING_SYMBOL,
SUM(TRADE_SIZE*TRADE_PRICE)/SUM(TRADE_SIZE) as VOLUME_WEIGHTED_PRICE
FROM STOCK_TRADE
WHERE TRADE_TIME BETWEEN '2005-11-14 12:00' AND '2005-11-14 15:00'
AND TRADING_SYMBOL = 'ADV'
GROUP BY TRADING_SYMBOL
Example output, and answer for 5 hour range starting at 0900;

+--------------+------------+-----------------------------+
| VWAP         | Date       | Interval                    |
+--------------+------------+-----------------------------+
| 296.20632365 | 11/10/2010 | Start (09:00) - End (14:00) |
+--------------+------------+-----------------------------+
Or

+--------------+------------+----------------+
| VWAP         | Date       | Start  | End   |
+--------------+------------+----------------+
| 296.20632365 | 11/10/2010 | 09:00  | 14:00 |
+--------------+------------+----------------+
IDENTIFYING THE BIGGEST DAILY TRADING RANGES � Challenge 2 of 2
Click here to download sample data set

This dataset contains pricing information for IBM across two weeks in June 2006.

Create a SQL script to determine the 3 days on which there was the biggest range between opening and closing prices.

Your output should display the following items;

Date 1 ; Date 2 ; Date 3 in descending order (biggest range first, second biggest range next etc)
The trading range per date
The time during the day that the stock reached its maximum price.
Note the date format in the current version of the dataset.